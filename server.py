PACKET_SETCOLOR = 0xFF
PACKET_SETRED = 0xFE
PACKET_SETBLUE = 0xFD
PACKET_SETGREEN = 0xFC

SERVER_PORT = 5654

from color import Color

import socket
import threading

class Server:
	running = True

	def __init__(self, lights):
		self.lights = lights

	def start(self):
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sock.bind(("", SERVER_PORT))
		
		self.running = True
		
		self.thread = threading.Thread(target=self.run, args=[])
		self.thread.start()
	
	def run(self):
		print "Server started."
		try:
			while self.running:
				data, addr = self.sock.recvfrom(1024)
				
				#print (data)
				
				if ord(data[0]) == PACKET_SETCOLOR:
					colors = data[1:].split(";")
					self.lights.setColor(Color(int(colors[0]), int(colors[1]), int(colors[2])))
				elif ord(data[0]) == PACKET_SETRED:
					color = int(data[1:])
					self.lights.setColor(Color(color, self.lights.lastColor.getGreen(), self.lights.lastColor.getBlue()))
				elif ord(data[0]) == PACKET_SETGREEN:
					color = int(data[1:])
					self.lights.setColor(Color(self.lights.lastColor.getRed(), color, self.lights.lastColor.getBlue()))
				elif ord(data[0]) == PACKET_SETBLUE:
					color = int(data[1:])
					self.lights.setColor(Color(self.lights.lastColor.getRed(), self.lights.lastColor.getGreen(), color))
				else:
					print "Server received unknown packet: " + data[0]
		except Exception, e:
			print(str(e))
				
		print "Server closed."
		
	def close(self):
		self.running = False
		try:
			self.sock.shutdown(socket.SHUT_RDWR)
		except Exception:
			pass
		self.sock.close();