#!/usr/bin/python

from light_manager import LightManager
from server import Server
from pushbullet import NotificationListener
import time

lights = LightManager()
server = Server(lights)
notifications = NotificationListener(lights)

print "RGB Controller by zachcheatham\n"

lights.lights.initialize()

server.start()
notifications.start()

while True:
	try:
		command = raw_input("")
		args = command.split(" ")
		
		if (args[0] == "exit"):
			break;
		elif (args[0] == "color"):
			if (len(args) < 4):
				print "Usage: color <r> <g> <b>"
			else:
				lights.setColor(Color(int(args[1]), int(args[2]), int(args[3])))
		elif (args[0] == "test"):
			lights.lights.selfTest()
		else:
			print "Unknown command."
	except KeyboardInterrupt:
		break
	except:
		pass

lights.cleanup()
notifications.close()
server.close()