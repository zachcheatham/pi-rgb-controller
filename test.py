#!/usr/bin/python

sequence = {"type": "steps", "repeat": 0, "steps": [
			{
				"delay": 0,
				"step": {
					"type": "color",
					"color": [0, 0, 0]
				}
			},
			{
				"delay": 0,
				"step": {
					"type": "steps",
					"repeat": blinkTimes,
					"steps": [
						{
							"delay": 0.5,
							"step": {
								"type": "color",
								"color": color.toList()
							}
						},
						{
							"delay": 0.5,
							"step": {
								"type": "color",
								"color": Colors.BLACK.toList()
							}
						}
					]
				}
			}
		]}