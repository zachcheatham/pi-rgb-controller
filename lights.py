from color import Color
from color import Colors
import RPi.GPIO as GPIO
import sys
import time

RED_PIN = 11
GREEN_PIN = 12
BLUE_PIN = 13

class Lights:
	pmw_r = None
	pmw_g = None
	pmw_b = None
	currentColor = Colors.BLACK
	
	def initialize(self):
		GPIO.setmode(GPIO.BOARD)
		GPIO.setup(RED_PIN, GPIO.OUT)
		GPIO.setup(GREEN_PIN, GPIO.OUT)
		GPIO.setup(BLUE_PIN, GPIO.OUT)
		self.pmw_r = GPIO.PWM(RED_PIN, 120)
		self.pmw_r.start(0)
		self.pmw_g = GPIO.PWM(GREEN_PIN, 120)
		self.pmw_g.start(0)
		self.pmw_b = GPIO.PWM(BLUE_PIN, 120)
		self.pmw_b.start(0)
		print"Lights initiated."

	def selfTest(self):
		sys.stdout.write("Performing light self test... ")
		sys.stdout.flush()
		
		self.setColor(Colors.RED)
		time.sleep(0.5)
		self.setColor(Colors.GREEN)
		time.sleep(0.5)
		self.setColor(Colors.BLUE)
		time.sleep(0.5)
		self.setColor(Colors.BLACK)
		
		print "Complete."
		
	def setColor(self, color):
		self.currentColor = color
	
		pwmValues = color.getPWMValues()
		
		self.pmw_r.ChangeDutyCycle(pwmValues[0])
		self.pmw_g.ChangeDutyCycle(pwmValues[1])
		self.pmw_b.ChangeDutyCycle(pwmValues[2])
		
	def cleanup(self):
		GPIO.output(RED_PIN, GPIO.LOW)
		GPIO.output(GREEN_PIN, GPIO.LOW)
		GPIO.output(BLUE_PIN, GPIO.LOW)
		GPIO.cleanup()
		print "Lights terminated."