from websocket import create_connection
from color import Colors
from color import Color
import traceback
import threading
import json
import time

class NotificationListener:
	running = True

	def __init__(self, lights):
		self.lights = lights
	
	def start(self):
		self.running = True
		
		try:
			self.wsock = create_connection("wss://stream.pushbullet.com/websocket/XooJG4zLbq9sgWBHlmFRW0tPnbQzaPDn")
		except Exception as e:
			self.lights.setProblem(True)
			self.close()
			
			print "Unable to connect to PushBullet. Attempting reconnect in 30 seconds."
			
			reconnectThread = threading.Thread(target=self.attemptReconnect, args=[])
			reconnectThread.start()
			
			return

		print "Connected to PushBullet."
		self.lights.setProblem(False)
		
		self.thread = threading.Thread(target=self.run, args=[])
		self.thread.start()
	
	def attemptReconnect(self):
		time.sleep(30)
		self.start()
	
	def run(self):
		print "Listening for notifications..."
		try:
			while self.running:
				raw = self.wsock.recv()

				data = json.loads(raw)
				if data["type"] == "push" and data["push"]["type"] == "mirror":
					self.handleLightPattern(data["push"]["package_name"])
		except Exception, e:
			if hasattr(e, "errno") and e.errno == 110:
				print "Lost connection to PushBullet."
				self.lights.setProblem(True)
				self.close()
				self.start()
			else:
				print traceback.format_exception_only(type(e), e)
			
			
	def close(self):
		self.running = False
		if hasattr(self, "wsock"):
			self.wsock.close()
		
		print "Closed connection to PushBullet."
		
	def handleLightPattern(self, package):
		color = Colors.WHITE
		
		if package == "com.pushbullet.android":
			color = Colors.GREEN
		elif package == "com.google.android.talk":
			color = Colors.GREEN
		elif package == "com.google.android.gm" or package == "com.google.android.apps.inbox":
			color = Color(255, 96, 96)
		elif package == "com.snapchat.android":
			color = Color(255, 255, 0)
		elif package == "com.skype.raider":
			color = Color(12, 190, 243)
		elif package == "kik.android":
			color = Colors.GREEN
		elif package == "com.facebook.katana":
			color = Colors.BLUE
		elif (package == "com.cyanogenmod.updater" or
			package == "com.android.chrome" or
			package == "com.android.deskclock"):
			return
		else:
			print "Unhandled application: " + package
		
		self.lights.doNotification(color, 3)