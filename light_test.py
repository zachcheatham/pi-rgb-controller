#!/usr/bin/python

from lights import Lights
from light_sequencer import LightSequencer

import time

lights = Lights()
lights.initialize()
lights.selfTest()

sequencer = LightSequencer(lights)

sequence = {
			"type": "steps",
			"repeat": 0,
			"steps": [
				{
					"delay": 0,
					"step": {
						"type": "color",
						"color": [0, 0, 0]
					}
				},
				{
					"delay": 0,
					"step": {
						"type": "steps",
						"repeat": 3,
						"steps": [
							{
								"delay": 0,
								"step": {
									"type": "fade",
									"colorStart": [0, 0, 0],
									"colorStop": [255, 100, 0],
									"time": 0.5
								}
							},
							{
								"delay": 0,
								"step": {
									"type": "fade",
									"colorStart": [255, 100, 0],
									"colorStop": [0, 0, 0],
									"time": 0.5
								}
							}
						]
					}
				}
			]
		}

sequencer.executeSequence(sequence, None)

time.sleep(30)
sequencer.stopSequence()

while sequencer.isRunning():
	print "Waiting for sequencer to end..."
	time.sleep(0.1)

lights.cleanup()