def colorToPercent(value):
	if value > 255:
		value = 255
	elif value < 0:
		value = 0
	
	return value / 255.0 * 100.0

class Color:
	def __init__(self, red, green, blue):
		self.red = red
		self.green = green
		self.blue = blue
	
	def getRed(self):
		return self.red
	
	def getGreen(self):
		return self.green
		
	def getBlue(self):
		return self.blue
		
	def toList(self):
		return [self.red, self.green, self.blue]
		
	def getValues(self):
		return (self.red, self.green, self.blue)
		
	def getPWMValues(self):
		return (colorToPercent(self.red), colorToPercent(self.green), colorToPercent(self.blue))
		
class Colors:
	BLACK = Color(0, 0, 0)
	WHITE = Color(255, 255, 255)
	RED = Color(255, 0, 0)
	GREEN = Color(0, 255, 0)
	BLUE = Color(0, 0, 255)
