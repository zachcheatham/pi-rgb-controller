from color import Color
from color import Colors
from lights import Lights
from light_sequencer import LightSequencer
import time

class LightManager:
	lights = Lights()
	sequencer = LightSequencer(lights)
	lastColor = Colors.BLACK
	notifying = False
	problem = False
	
	def setColor(self, color):
		self.lastColor = color
		if self.notifying == False and self.problem == False:
			self.lights.setColor(color)
		
	def setProblem(self, problem):
		self.problem = problem
		
		if not problem:
			if self.notifying == False:
				self.lights.setColor(self.lastColor)
		else:
			if self.notifying == False:
				self.lights.setColor(Color(10, 0, 0))
		
	def doNotification(self, color, blinkTimes):
		#self.notifying = True
		
		sequence = {
			"type": "steps",
			"repeat": 0,
			"steps": [
				{
					"delay": 0,
					"step": {
						"type": "color",
						"color": [0, 0, 0]
					}
				},
				{
					"delay": 0,
					"step": {
						"type": "steps",
						"repeat": blinkTimes,
						"steps": [
							{
								"delay": 0,
								"step": {
									"type": "fade",
									"colorStart": Colors.BLACK.toList(),
									"colorStop": color.toList(),
									"time": 0.5
								}
							},
							{
								"delay": 0,
								"step": {
									"type": "fade",
									"colorStart": color.toList(),
									"colorStop": Colors.BLACK.toList(),
									"time": 0.5
								}
							}
						]
					}
				}
			]
		}
		
		self.sequencer.executeSequence(sequence, self.notificationFinished)
	
	def notificationFinished(self):
		self.notifying = False
		
		if self.problem == True:
			self.lights.setColor(Color(10, 0, 0))
		else:
			self.lights.setColor(self.lastColor)
	
	def cleanup(self):
		if self.sequencer.isRunning():
			self.sequencer.stopSequence()
			while self.sequencer.isRunning():
				time.sleep(0.1)
		
		self.lights.cleanup()