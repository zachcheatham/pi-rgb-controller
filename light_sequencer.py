#from lights import Lights
from color import Color

import json
import time
import threading

class LightSequencer:
	sequenceRunning = False
	threadRunning = False
	
	def __init__(self, lights):
		self.lights = lights
		
	def executeSequence(self, command, callback):
		if not self.threadRunning:
			sequence = False
			if isinstance(command, str):
				sequence = json.loads(command)
			else:
				sequence = command
				
			thread = threading.Thread(target=self.doSequence, args=[sequence, callback])
			self.threadRunning = True
			thread.start()
		else:
			print "Warning: Tried to start sequence while another was running!"
		
	def stopSequence(self):
		self.sequenceRunning = False
		
	def isRunning(self):
		return self.threadRunning
		
	def doSequence(self, sequence, callback):
		self.sequenceRunning = True
		self.handleCommand(sequence)
		print "Debug: Sequence ended."
		self.sequenceRunning = False
		self.threadRunning = False
		
		if callback != None:
			callback()
		
	def handleCommand(self, command):
		if self.sequenceRunning == True:
			if command["type"] == "steps":
				repeat = int(command["repeat"])
				self.handleSteps(repeat, command["steps"])
			elif command["type"] == "color":
				self.lights.setColor(Color(int(command["color"][0]), int(command["color"][1]), int(command["color"][2])))
			elif command["type"] == "fade":
				startColor = Color(int(command["colorStart"][0]), int(command["colorStart"][1]), int(command["colorStart"][2]))
				endColor = Color(int(command["colorStop"][0]), int(command["colorStop"][1]), int(command["colorStop"][2]))
				seconds = float(command["time"])
				self.fadeLights(startColor, endColor, seconds)
			else:
				print "Sequencer received unknown command type: {}".format(command["type"])
				
	def handleSteps(self, repeat, steps):
		if repeat >= 0:
			for i in range(0, repeat + 1):
				# Stop if we were interrupted
				if self.sequenceRunning:
					# Handle each step
					for i in range(len(steps)):
						# Make sure we haven't been stopped
						if self.sequenceRunning:
							self.handleStep(steps[i])
		else:
			while self.sequenceRunning:
				for i in range(len(steps)):
					# Make sure we haven't been stopped
					if self.sequenceRunning:
						self.handleStep(steps[i])
				
					
	def handleStep(self, step):
		# We really like to be running
		if self.sequenceRunning:
			delayTime = float(step["delay"])
			if delayTime > 0:
				time.sleep(delayTime)
				
			self.handleCommand(step["step"])
			
	def fadeLights(self, startColor, endColor, seconds):
		# Debug for timing purposes
		#startTime = time.time() * 1000
	
		# Difference in color
		redDifference = startColor.getRed() - endColor.getRed()
		greenDifference = startColor.getGreen() - endColor.getGreen()
		blueDifference = startColor.getBlue() - endColor.getBlue()
		
		# How many iterations
		steps = abs(redDifference)
		if abs(greenDifference) > steps:
			steps = abs(greenDifference)
		if abs(blueDifference) > steps:
			steps = abs(blueDifference)
		
		# How much to change between iterations
		redStep = redDifference / float(steps) * -1
		greenStep = greenDifference / float(steps) * -1
		blueStep = blueDifference / float(steps) * -1
		
		# How long to pause between iterations
		sleepTime = seconds / steps

		# Keep track of color
		red = float(startColor.getRed())
		green = float(startColor.getGreen())
		blue = float(startColor.getBlue())
	
		# Finish debug
		#print "Fade setup took {}ms".format(time.time() * 1000 - startTime)
		
		# Debug for timing purposes
		#startTime = time.time() * 1000
	
		# Iterate
		for i in range(0, steps):
			# Stop if we were interrupted
			if self.sequenceRunning == False:
				return
				
			# Set the color
			#print "{}, {}, {}".format(red, green, blue)
			self.lights.setColor(Color(red, green, blue))
			
			# Step all colors
			red += redStep
			green += greenStep
			blue += blueStep
				
			# Pause
			time.sleep(sleepTime)
		
		# One last step
		self.lights.setColor(endColor)
		
		# Finish debug
		#print "Fade took {}ms".format(time.time() * 1000 - startTime)